import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').type('Ola Normann'); 

    cy.get('#address').type('Gate navn 42')

    cy.get('#postCode').type('6942')

    cy.get('#city').type('By')

    cy.get('#creditCardNo').type('1234567812345678')
    
});
When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').clear().blur(); 
    cy.get('#fullNameError').should('exist');

    cy.get('#address').clear().blur(); 
    cy.get('#addressError').should('exist');

    cy.get('#postCode').clear().blur(); 
    cy.get('#postCodeError').should('exist');

    cy.get('#city').clear().blur(); 
    cy.get('#cityError').should('exist');

    cy.get('#creditCardNo').clear().blur(); 
    cy.get('#creditCardNoError').should('exist');
    
});


Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').should('exist');
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});
And(/^trykker på Fullfør kjøp$/, function () {
    cy.get('input[type="submit"]').should('have.value', 'Fullfør handel').click();
});
